#!/usr/bin/python3
from vector3 import Vector3


def main():
    v = Vector3.origin()
    print(v)
    print(hash(v))


if __name__ == '__main__':
    main()
